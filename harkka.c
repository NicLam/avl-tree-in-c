/*
 * Tietorakenteet ja algoritmit BM40A0301
 * Niclas Lamponen 0455946
 * 30.11.2017
 * Lähteenä käytetty moodle-sivuilla luentomateriaaleissa esitettyä AVL-puun mallia
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int key;
    int state;
    struct node *left;
    struct node *right;
} node, *treeptr;

void addNode(treeptr *, int, int *);
void rightRotate(treeptr *, int *);
void leftRotate(treeptr *, int *);
void freeTree(treeptr);
void findNode(treeptr, int);
void printLevel(treeptr, int, int, int);
void printTree(treeptr);
int getTreeDepth(treeptr);


int main() {

    /* Pointer to the root of the tree. Variables to help with menu (tmp, key, i), unbalance factor and pointer to file */
    treeptr tree = NULL;
    int tmp, key, unbalanced = 0;
    FILE *fp = fopen("nodes.txt", "r");
    printf("Rakennetaan puuta tiedostosta..\n");

    /* Read file, create initial tree out of the entries in nodes.txt */
    while (fscanf(fp, "%d", &tmp) == 1) {
        addNode(&tree, tmp, &unbalanced);
    }
    fclose(fp);
    printf("\n");

    /* Menu */
    while (1) {
        printf("0) Lopeta ohjelma\n");
        printf("1) Lisää alkio\n");
        printf("2) Tulosta puu\n");
        printf("3) Etsi alkio\n");
        printf("4) Vapauta nykyinen puu\n");
        printf("Valinta: ");
        scanf("%d", &tmp);
        switch (tmp) {
        case 0:
            printf("Vapautetaan puu ja lopetetaan..\n\n");
            freeTree(tree);
            exit(1);
            break;
        case 1:
            printf("Syötä puuhun lisättävä alkio: ");
            scanf("%d", &key);
            printf("Puu ennen kiertoa:\n");
            printTree(tree);
            addNode(&tree, key, &unbalanced);

            printf("Puu lisäyksen jälkeen\n");
            printTree(tree);
            break;
        case 2:
            printTree(tree);
            break;
        case 3:
            printf("Syötä etsittävä alkio: ");
            scanf("%d", &key);
            findNode(tree, key);
            break;
        case 4:
            printf("Vapautetaan puu..\n\n");
            tree = NULL; /* Reset variables */
            unbalanced = 0;
            break;
        default:
            printf("Väärä valinta\n\n");
            break;
        }
    }
    return 0;
}

void addNode(treeptr *parent, int key, int *unbalanced) {

    /* Create a new node at root/at the end of the branch */
    if (!(*parent)) {
        if (!(*parent = (treeptr)malloc(sizeof(node)))) {
            perror("Muistin varaus epäonnistui\n");
            exit(1);
        }

        printf("Lisätään puuhun %d..\n", key);

        /* Set values for new node */
        (*parent)->left  = NULL;
        (*parent)->right = NULL;
        (*parent)->state = 0;
        (*parent)->key   = key;
        *unbalanced      = 1;

    } else if ((*parent)->key > key) {
        /* Recursively traverse tree until reaching the end of the branch */
        addNode(&(*parent)->left, key, unbalanced);

        if (*unbalanced) {
            switch ((*parent)->state) {
            case -1: /* Tree is right heavy */
                (*parent)->state = 0;
                *unbalanced      = 0;
                break;
            case 0: /* Tree is balanced */
                (*parent)->state = 1;
                break;
            case 1: /* Tree is left heavy */
                rightRotate(parent, unbalanced);
            }
        }

    } else if ((*parent)->key < key) {

        addNode(&(*parent)->right, key, unbalanced);

        if (*unbalanced) {
            switch ((*parent)->state) {
            case -1: /* Tree is right heavy */
                leftRotate(parent, unbalanced);
                break;
            case 0: /* Tree is balanced */
                (*parent)->state = -1;
                break;
            case 1: /* Tree is left heavy */
                (*parent)->state = 0;
                *unbalanced = 0;
            }
        }
    } else {
        printf("Luku on jo puussa\n");
    }
}

void leftRotate(treeptr *parent, int *unbalanced) {
    treeptr child, gchild;
    child = (*parent)->right;

    if (child->state == -1) { /* LL (left) -rotate, create new links */
        printf("LL -kierto\n");
        (*parent)->right = child->left;
        child->left      = *parent;
        (*parent)->state = 0;
        (*parent)        = child;
    } else { /* LR (Left-right) -rotate, create new links */
        printf("LR -kierto\n");
        gchild           = child->left;
        child->left      = gchild->right;
        gchild->right    = child;
        (*parent)->right = gchild->left;
        gchild->left     = *parent;

        /* Set states to correspond new structure */
        switch (gchild->state) {
        case -1:
            (*parent)->state = 0;
            child->state     = -1;
            break;
        case 0:
            (*parent)->state = 0;
            child->state     = 0;
            break;
        case 1:
            (*parent)->state = 1;
            child->state     = 0;
        }
        *parent = gchild;
    }

    /* Reset parent & unbalanced states since tree is now balanced */
    (*parent)->state = 0;
    *unbalanced      = 0;
}

void rightRotate(treeptr *parent, int *unbalanced) {
    treeptr child, gchild;
    child = (*parent)->left;

    if (child->state == 1) { /* RR (right) -rotate, create new links */
        printf("RR -kierto\n");
        (*parent)->left  = child->right;
        child->right     = *parent;
        (*parent)->state = 0;
        (*parent)        = child;
    } else { /* RL Right-left) -rotate, create new links */
        printf("RL -kierto\n");
        gchild           = child->right;
        child->right     = gchild->left;
        gchild->left     = child;
        (*parent)->left  = gchild->right;
        gchild->right    = *parent;

        switch (gchild->state) {
        case -1:
            (*parent)->state = 0;
            child->state     = 1;
            break;
        case 0:
            (*parent)->state = 0;
            child->state     = 0;
            break;
        case 1:
            (*parent)->state = -1;
            child->state     = 0;
        }
        *parent = gchild;
    }

    /* Reset parent & unbalanced states since tree is now balanced */
    (*parent)->state = 0;
    *unbalanced      = 0;
}

void findNode(treeptr treenode, int key) {
    if (!treenode) {
        printf("Hakemaasi avainta ei löytynyt\n\n");
        return;
    }
    if (treenode->key == key)
        printf("Avain %d on puussa\n\n", key);
    if (treenode->key < key) {
        findNode(treenode->right, key);
    } else if (treenode->key > key) {
        findNode(treenode->left, key);
    }
}


void freeTree(treeptr treenode) {
    if (treenode != NULL) {
        freeTree(treenode->right);
        freeTree(treenode->left);
        free(treenode);
    }
}


void printLevel(treeptr treenode, int desired, int current, int depth) {
    int k = 1, i;
    if (desired == current) {
        if (treenode) {
            printf("%03d", treenode->key);
        } else {
            printf("   "); /* No node, print empty */
        }

        for (i = 0; i < depth - current; i++, k*=2); /* k-1 is number of 'invisible blocks (block is the space 3 numbers occupy)' (Math.pow) */
        for (i = 0; i < k - 1; i++) {
            printf("    "); /* 3 + 1 for the 'invisible block' and gap */
        }
        printf(" "); /* gap on the other side of the block */

    } else {
        if (!treenode) {
            printLevel(NULL, desired, current + 1, depth);
            printLevel(NULL, desired, current + 1, depth);
        } else {
            printLevel(treenode->left, desired, current + 1, depth);
            printLevel(treenode->right, desired, current + 1, depth);
        }
    }
}

int getTreeDepth(treeptr treenode) {
    if (!treenode)
        return 0;
    else {
        /* recursively traverse tree to the deeper branch */
        int leftSubtreeDepth = getTreeDepth(treenode->left);
        int rightSubtreeDepth = getTreeDepth(treenode->right);

        /* +1 to include root node to the height of the subtree */
        if (leftSubtreeDepth > rightSubtreeDepth) return leftSubtreeDepth + 1;
        else return rightSubtreeDepth + 1;
    }
}


void printTree(treeptr tree) {
    int depth, i, k, l, h;
    depth = getTreeDepth(tree);
    i = getTreeDepth(tree) - 1; /* 2nd last level */

    /* h is initialized just to help with printing. i goes through tree level by level */
    for (h = 0; i >= 0; i--, h++) {
        /* print padding to the left side of the first node */
        k = 1;
        /* k-1 is the amount of empty spaces to be printed before the tree */
        for (l = h; l < depth; l++, k *=2);
        for (l = 0; l < k - 1; ++l)
            printf(" ");

        printLevel(tree, depth - i, 1, depth);
        printf("\n");
    }
    printf("\n");
}
